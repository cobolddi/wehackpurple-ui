<?php @include('template-parts/header.php') ?>

<section class="BlogsPost BlogPageCards">
	<div class="container">
		<div class="Heading">
			<h2 class="HeadingwithYellowBorder">We Hack Purple Blog</h2>
		</div>
		<div class="row">
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Security is everybody's job</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Another blog title here</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A third blog title will help us to design things</h6>
							<p>Oh, you read this small stuff. Kudos to you! But why are you still reading this? All ok?</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A fourth blog title that is really long and takes three lines to fit into this area.</h6>
							<p>The website will adjust, don't worry. Still, why are you reading this?</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Security is everybody's job</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Another blog title here</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A third blog title will help us to design things</h6>
							<p>Oh, you read this small stuff. Kudos to you! But why are you still reading this? All ok?</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A fourth blog title that is really long and takes three lines to fit into this area.</h6>
							<p>The website will adjust, don't worry. Still, why are you reading this?</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Security is everybody's job</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>Another blog title here</h6>
							<p>Push your web development skills to the next level, through expert screencasts on Laravel, Vue, and so much more.</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A third blog title will help us to design things</h6>
							<p>Oh, you read this small stuff. Kudos to you! But why are you still reading this? All ok?</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-3">
				<a href="#">
					<div class="cards">
						<img src="assets/img/tempImg/security-1.png" alt="">
						<div class="BottomContent">
							<h6>A fourth blog title that is really long and takes three lines to fit into this area.</h6>
							<p>The website will adjust, don't worry. Still, why are you reading this?</p>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="CtaBlock">
			<a href="#" class="PurpleYellowBtn Prev"><img src="assets/img/leftarw.svg">Previous Page</a>
			<a href="#" class="PurpleYellowBtn Next">Next Page<img src="assets/img/rightarw.svg"></a>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>