$(document).ready(function() {	

	$('.HomeBanner').slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
        adaptiveHeight: true,
        cssEase: 'ease',
    	 easing: 'swing',
        responsive: [{
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 400,
           settings: {
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });


    $('a.dismiss').on('click', function(e) {
        $('body').addClass('close');
        e.stopPropagation();
        e.preventDefault();
    });


    $(".set > a").on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
          $(this)
            .siblings(".content")
            .slideUp(200);
          $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
        } else {
          $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
          $(this)
            .find("i")
            .removeClass("fa-plus")
            .addClass("fa-minus");
          $(".set > a").removeClass("active");
          $(this).addClass("active");
          $(".content").slideUp(200);
          $(this)
            .siblings(".content")
            .slideDown(200);
        }
    });



});


// $(window).scroll(function() {    
//     var scroll = $(window).scrollTop();

//     if (scroll >= 20) {
//         $("header").addClass("HeaderBg");
//     } else {
//         $("header").removeClass("HeaderBg");
//     }
// });
$(document).ready(function() {
  $("#toggle").click(function() {
    var elem = $("#toggle").text();
    if (elem == "Read complete transcript") {
      //Stuff to do when btn is in the read more state
      $("#toggle").text("Read Less");
      $("#text").slideDown();
    } else {
      //Stuff to do when btn is in the read less state
      $("#toggle").text("Read complete transcript");
      $("#text").slideUp();
    }
  });
});


$(document).ready(function(){
    $(".button a").click(function(e){
        e.preventDefault();
        $(".overlay").fadeToggle(200);
        $("body").addClass("overflowhidden");
       $(this).toggleClass('btn-open').toggleClass('btn-close');
    });
});
$('.close').on('click', function(e){
    e.preventDefault();
    $(".overlay").fadeToggle(200); 
    $("body").removeClass("overflowhidden");  
    $(".button a").toggleClass('btn-open').toggleClass('btn-close');
    open = false;
});


$(document).on('click','.js-videoPoster',function(e) {
  e.preventDefault();
  var poster = $(this);
  var wrapper = poster.closest('.js-videoWrapper');
  videoPlay(wrapper);
});

function videoPlay(wrapper) {
  var iframe = wrapper.find('.js-videoIframe');
  // Берем ссылку видео из data
  var src = iframe.data('src');
  // скрываем постер
  wrapper.addClass('videoWrapperActive');
  // подставляем в src параметр из data
  iframe.attr('src',src);
}