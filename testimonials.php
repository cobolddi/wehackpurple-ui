<?php @include('template-parts/header.php') ?>

<section class="BlogsPost BlogPageCards Testimonials">
	<div class="container">
		<div class="Heading">
			<h2 class="HeadingwithYellowBorder">Testimonials</h2>
		</div>
		<div class="testimonialsWrapper">
			<div class="row">
				<div class="col-12 col-md-7">
					<div class="TestimonialCard">
						<h4>Andrea</h4>
						<p>I'd been trying to learn Application Security topics on my own, but Tanya's (We Hack Purple) Application Security Fundamentals course made a lot of topics much clearer to me and helped me to understand what to prioritize when building an AppSec program. The lessons are short and easily "digestible" so that you can stop and resume as needed, without feeling like you have forgotten something.</p>
						<p>I really appreciated her perspective from both sides (Security and Developer) because having both perspectives, she is able to teach you how to best communicate most effectively. I highly recommend the course - and the book is absolutely essential! I am so glad to have it as a reference, and I will refer to it often in my new position (which I wouldn't have had the courage to go for without the knowledge that this course provided.)</p>
					</div>
				</div>
				<div class="col-12 col-md-5">
					<div class="row">
						<div class="col-12">
							<div class="TestimonialCard">
								<h4>Ronald</h4>
								<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
							</div>
						</div>
						<div class="col-12">
							<div class="TestimonialCard">
								<h4>Benita</h4>
								<p>Tanya is a wonderful person with fantasic techniques to teach AppSec. I felt that all the materials, videos, assignments and quizes were pretty straightforward and easy to complete. I really enjoyed the course.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="TestimonialCard">
						<h4>Ronald</h4>
						<p>The engaging presentation skills of Tanya as instructor keep it fun, light, and almost conversational. The exercises compel you to think aspects of AppSec which one might have not thought of. Case studies really help strengthen understanding and open the doors of perception to new possibilities</p>
					</div>
				</div>
			</div>
		</div>	
		<div class="CtaBlock">
			<a href="#" class="PurpleYellowBtn">Load more testimonials</a>
		</div>
		<div class="Newsletter">
			<div class="Content">
				<img src="assets/img/shadowlogo.svg" alt="">
				<h2>Join our newsletter, you won’t regret it!</h2>
				<p>Join our newsletter to receive free content, deals, invites, advance notice of new products, and so much more.</p>
				<form action="">
					<input type="email" placeholder="@ Enter your email address">
					<input type="submit" value="Join">
				</form>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>