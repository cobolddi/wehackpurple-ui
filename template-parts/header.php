<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>We Hack Purple</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/logo.svg">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="Hotlink">
			<div class="container">
				<div class="row">
					<div class="col-10 col-md-10">
						<p>A ticker area for special highlights. This will come with <a href="shipping.php"> <marquee>highlighted links!</marquee></a></p>
					</div>
					<div class="col-2 col-md-2">
						<div class="Dismiss">
							<a href="#" class="dismiss"><img src="assets/img/close.svg" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="HeaderWrapper">
			<div class="container">
				<div class="row">
					<div class="col-3 col-md-3">
						<div class="LogoBox">
							<a href="index.php" class="logo"><img src="assets/img/logo-1.svg"></a>
						</div>
					</div>
					<div class="col-9 col-md-9 DesktopOnly">
						<div class="DesktopNav">
							<div class="NavigationMenu">
								<ul>
									<li><a href="">Academy</a></li>
									<li><a href="">Community</a></li>
									<li><a href="podcast.php">Podcast</a></li>
									<li><a href="">Shop</a></li>
									<li><a href="blogs.php">Blogs</a></li>
									<li><a href="testimonials.php">Testimonials</a></li>
									<li><a href="">About</a></li>
									<li><a href="faq.php">FAQ</a></li>
									<li><a href="contactus.php">Contact</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-9 col-md-9 MobileOnly">
						<div class="button">
							<a class="btn-open" href="#"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

<div class="overlay">
	<div class="LogoBox">
		<div class="container">
			<div class="row">
				<div class="col-3">
					<a href="index.php" class="logo"><img src="assets/img/logo-1.svg"></a>
				</div>
				<div class="col-9">
					<a href="#" class="close"><img src="assets/img/close.svg" alt=""></a>
				</div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<ul>
			<li><a href="">Academy</a></li>
			<li><a href="">Community</a></li>
			<li><a href="podcast.php">Podcast</a></li>
			<li><a href="">Shop</a></li>
			<li><a href="blogs.php">Blogs</a></li>
			<li><a href="testimonials.php">Testimonials</a></li>
			<li><a href="">About</a></li>
			<li><a href="faq.php">FAQ</a></li>
			<li><a href="contactus.php">Contact</a></li>
		</ul>
	</div>
	<ul class="social">
		<li class="facebook"><a href="#"><img src="assets/img/facebook-white.svg" alt=""></a></li>
		<li class="twitter"><a href="#"><img src="assets/img/twitter-white.svg" alt=""></a></li>
		<li class="youtube"><a href="#"><img src="assets/img/youtube.svg" alt=""></a></li>
		<li class="insta"><a href="#"><img src="assets/img/insta-white.svg" alt=""></a></li>
	</ul>
</div>

	<!-- <div class="container"> -->
	<main>