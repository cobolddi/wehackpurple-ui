<section class="PodcastCardsBlock">
	<div class="container">
		<div class="CardsWrapper">
			<div class="TopHeading">
				<h2 class="HeadingwithYellowBorder">We Hack Purple Podcast</h2>
				<p>The weekly podcast is live every Thursday from 6:00-7:00 pm PST on YouTube, then released in audio-only to every major podcast platform. We feature a diverse range of guests for all walks of InfoSec, to talk about their careers, jobs, and how they got to where they are today. Featuring host Tanya Janca.</p>
				<p>Join us to explore your career in information security!</p>
			</div>
			<div class="SearchPodcast">
				<div class="row">
					<div class="col-12 col-md-8">
						<ul>
							<li class="youtube"><a href="#"><img src="assets/img/youtube.svg" alt=""></a><span>Subscribe on Youtube</span></li>
						</ul>
					</div>
					<div class="col-12 col-md-4">
						<form action="">
							<input type="search" placeholder="Search Podcasts">
							<input type="submit" value="">
						</form>
					</div>
				</div>
			</div>
			<div class="Cards">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="ImgBlock">
							<img src="assets/img/tempImg/podcast1.png" alt="">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="RightContent">
							<h3>Episode 22 with Guest Talesh Seeparsan</h3>
							<h6>Thursday January 28th 2021, 6:00 pm PST</h6>
							<p>Learn what it's like to be an eCommerce security educator and auditor, with Talesh Seeparsan! Originally an eCommerce PHP developer, Talesh has pivoted his career into eCommerce security. Specifically building more defensible eCommerce.</p>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<!-- <a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a> -->
							</div>
							<div class="BtnWithLogo">
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
								<img src="assets/img/threadfix.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Cards">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="ImgBlock">
							<img src="assets/img/tempImg/podcast2.png" alt="">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="RightContent">
							<h3>Episode 24 with Guest Stephanie Black</h3>
							<h6>Thursday January 28th 2021, 6:00 pm PST</h6>
							<p>Learn what it's like to be an eCommerce security educator and auditor, with Talesh Seeparsan! Originally an eCommerce PHP developer, Talesh has pivoted his career into eCommerce security. Specifically building more defensible eCommerce.</p>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<!-- <a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a> -->
							</div>
							<div class="BtnWithLogo">
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
								<img src="assets/img/threadfix.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Cards">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="ImgBlock">
							<img src="assets/img/tempImg/podcast3.png" alt="">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="RightContent">
							<h3>Episode 25 with Guest Troy Hunt</h3>
							<h6>Thursday January 28th 2021, 6:00 pm PST</h6>
							<p>Learn what it's like to be an eCommerce security educator and auditor, with Talesh Seeparsan! Originally an eCommerce PHP developer, Talesh has pivoted his career into eCommerce security. Specifically building more defensible eCommerce.</p>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<!-- <a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a> -->
							</div>
							<div class="BtnWithLogo">
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
								<img src="assets/img/threadfix.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Cards">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="ImgBlock">
							<img src="assets/img/tempImg/podcast4.png" alt="">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="RightContent">
							<h3>Episode 22 with Guest Barbara Schanchner</h3>
							<h6>Thursday January 28th 2021, 6:00 pm PST</h6>
							<p>Learn what it's like to be an eCommerce security educator and auditor, with Talesh Seeparsan! Originally an eCommerce PHP developer, Talesh has pivoted his career into eCommerce security. Specifically building more defensible eCommerce.</p>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<!-- <a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a> -->
							</div>
							<div class="BtnWithLogo">
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
								<img src="assets/img/threadfix.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="CtaBlock">
				<a href="#" class="PurpleYellowBtn">Load more podcasts</a>
			</div>
		</div>
	</div>
</section>