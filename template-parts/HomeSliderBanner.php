<section class="HomeBannerSlider">
  <div class="container">
  	<div class="SliderWithContent">
  		
  		<div class="HomeBanner">
    			<div class="slide-content">
    				<img src="assets/img/tempImg/banner.png" />
    				<div class="BannerTextContent">
    					<h1>Application Security Training</h1>
    					<p>Learn how to create software for the web that is as secure as Louis Lane with Superman!</p>
              <a href="#" class="GradientButton">Explore Courses</a>
    				</div>
    			</div>
    			<div class="slide-content">
    				<img src="assets/img/tempImg/banner.png" />
    				<div class="BannerTextContent">
    					<h1>Application Security Training</h1>
    					<p>Learn how to create software for the web that is as secure as Louis Lane with Superman!</p>
              <a href="#" class="GradientButton">Explore Courses</a>
    				</div>
    			</div>
    			<div class="slide-content">
    				<img src="assets/img/tempImg/banner.png" />
    				<div class="BannerTextContent">
    					<h1>Application Security Training</h1>
    					<p>Learn how to create software for the web that is as secure as Louis Lane with Superman!</p>
              <a href="#" class="GradientButton">Explore Courses</a>
    				</div>
    			</div>
  		</div>
  	</div>
  </div>
</section>