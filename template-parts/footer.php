
	</main>
	
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3">
					<a href="index.php" class="footer-logo"><img src="assets/img/logo.svg" alt=""></a>
				</div>
				<div class="col-12 col-md-9">
					<div class="FooterContent">
						<h5>Follow WeHackPurple</h5>
						<ul class="social">
							<li class="facebook"><a href="#"><img src="assets/img/facebook-white.svg" alt=""></a></li>
							<li class="twitter"><a href="#"><img src="assets/img/twitter-white.svg" alt=""></a></li>
							<li class="youtube"><a href="#"><img src="assets/img/youtube.svg" alt=""></a></li>
							<li class="insta"><a href="#"><img src="assets/img/insta-white.svg" alt=""></a></li>
						</ul>
						<div class="FooterMenu">
							<ul>
								<li><a href="">Academy</a></li>
								<li><a href="">Community</a></li>
								<li><a href="">Podcast</a></li>
								<li><a href="">Shop</a></li>
								<li><a href="">Testimonials</a></li>
								<li><a href="">About</a></li>
								<li><a href="">FAQ</a></li>
								<li><a href="">Contact</a></li>
							</ul>
						</div>
						<div class="BottomFooter">
							<p class="DesktopOnly">Copyright 2020 Janca Enterprises LLC</p>
							<ul>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Delivered by Cobold Digital</a></li>
							</ul>
							<p class="MobileOnly">Copyright 2020 Janca Enterprises LLC</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>