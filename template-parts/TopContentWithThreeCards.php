<section class="TopContentWithThreeCards">
	<div class="container">
		<div class="Centercontent">
			<h2 class="HeadingwithYellowBorder">We Hack Purple Podcast</h2>
			<p>The weekly podcast is live every Thursday from 6:00-7:00 pm PST on YouTube, then released in audio-only to every major podcast platform. We feature a diverse range of guests for all walks of InfoSec, to talk about their careers, jobs, and how they got to where they are today. Featuring host Tanya Janca. </p>
			<p>Join us to explore your career in information security!</p>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="Newsletter">
			<div class="Content">
				<img src="assets/img/shadowlogo.svg" alt="">
				<h2>Join our newsletter, you won’t regret it!</h2>
				<p>Join our newsletter to receive free content, deals, invites, advance notice of new products, and so much more.</p>
				<form action="">
					<input type="email" placeholder="@ Enter your email address">
					<input type="submit" value="Join">
				</form>
			</div>
		</div>
	</div>
</section>