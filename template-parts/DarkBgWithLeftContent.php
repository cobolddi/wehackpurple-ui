<div class="container">
	<section class="DarkBgWithLeftContent">
			<div class="row">
				<div class="col-12 col-md-7">
					<div class="LeftContent">
						<h2>We Hack Purple Community</h2>
						<p>The We Hack Purple Community is a safe and professional space for information security professionals to meet, network, discuss, and learn! Whether you are a seasoned security veteran, or brand new to the topic, by joining the We Hack Purple Community you will kickstart your learning, and super-charge your career.</p>
						<p>Inside the community you will find exclusive security content, invites to members-only events, a searchable knowledgeable, groups for you to discuss security with your peers, and so much more.</p>
						<div class="CtaBtns">
							<a href="#" class="GradientButton">Join the Community</a>
							<a href="#" class="SimpleBtn">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		<div class="OverflowImg">
			<img src="assets/img/community.svg" alt="">
		</div>
	</section>
</div>	