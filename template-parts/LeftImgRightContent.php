<section class="Section LeftImgRightContent">
	<div class="container">
		<div class="WhiteBgBlock">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="LeftImg">
						<img src="assets/img/tempImg/characters-final2.png" alt="">
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="RightContent">
						<h4>Join the purple revolution</h4>
						<p>The security industry needs YOU! Join us in securing the world’s software, by learning, watching, reading, sharing and leading! Create an application security program where you work, find your first job in information security, become an active part of our community, and make the internet a safer place for everyone.  </p>
						<p>We offer our textbook, Alice and Bob Learn Application Security, our courses in the Academy, our premium content, events and discussion forums in the community, our amazing conversations with our guests on the podcast, and so much more to get you learning all you need to know to be an amazing application security professional. With We Hack Purple you can continuously learn, all throughout your career!</p>
						<p>Join the revolution! Security all the things with We Hack Purple!</p>
					</div>
				</div>
			</div>
		</div>
		<div class="Centercontent">
			<h2 class="HeadingwithYellowBorder">We Hack Purple Academy</h2>
			<p>Learn how to create secure software, from industry experts. Our online courses teach application security theory and hands-on technical lessons. Whether you are a CISO that needs to get a better picture of your application security posture, a software developer that is concerned about creating secure software, or a person who wants to become an AppSec professional, we have something for you.</p>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/security-1.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>Application Security Foundations</h6>
							<p>Level 1</p>
							<div class="PriceLink">
								<span>$499</span>
								<a href="#">View Bundle <img src="assets/img/arw-right.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/security-2.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>Application Security Foundations</h6>
							<p>Level 1</p>
							<div class="PriceLink">
								<span>$499</span>
								<a href="#">View Bundle <img src="assets/img/arw-right.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/security-3.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>Incident Response Mini Course</h6>
							<p>Level 1</p>
							<div class="PriceLink">
								<span>$499</span>
								<a href="#">View Bundle <img src="assets/img/arw-right.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="CtaBlock">
			<a href="#" class="PurpleYellowBtn">Visit the Academy</a>
		</div>
	</div>
</section>