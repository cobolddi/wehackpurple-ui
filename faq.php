<?php @include('template-parts/header.php') ?>

<section class="FaqSection">
	<div class="container">
		<div class="TopHeading">
			<h2 class="HeadingwithYellowBorder">Frequently Asked Questions</h2>
		</div>
		<div class="AccordianBlock">
			<div class="set">
			    <a href="#">
			      What is We Hack Purple? 
			    </a>
			    <div class="content">
			      	<p>We are a Canadian company focused on helping anyone and everyone create secure software. We have an Academy with formal training programs, we offer live training (I will send you a PDF to link), we are a professional learning community, we have a podcast, newsletter, blog and LOTS of social media. We even have a swag shop, in case you want to look fantastic while you learn! We were founded by Tanya Janca, a great big security nerd.</p>
			      	<div class="video_wrapper video_wrapper_full js-videoWrapper">
						<iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen data-src="https://www.youtube.com/embed/G4cJ4wviwS8?autoplay=1&modestbranding=1&rel=0&hl=ru&showinfo=0&color=white"></iframe>
						<button class="videoPoster js-videoPoster"></button>
					</div>
			    </div>
			  </div>
			  <div class="set">
			    <a href="#">
			      What is application security (AppSec), and how can I learn it? 
			    </a>
			    <div class="content">
			      <p> Aliquam cursus vitae nulla non rhoncus. Nunc condimentum erat nec dictum tempus. Suspendisse aliquam erat hendrerit vehicula vestibulum.</p>
			    </div>
			  </div>
			<div class="set">
				<a href="#">
				  What is the We Hack Purple Community? Why should I join? 
				</a>
				<div class="content">
				  <p>Pellentesque aliquam ligula libero, vitae imperdiet diam porta vitae. sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
				</div>
			</div>
			<div class="set">
			    <a href="#">
			      What does your academy teach?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      Do you give live (virtual) training? 
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      Do you have a mailing list? 
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      How long will I have access to the courses that I bought from We Hack Purple? Is there a deadline to finish?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      When can I receive my certification for the AppSec Foundations Program?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      I’m trying to get into information security in general and I’m not sure how. Where do I start?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      I’m trying to get into application security and I’m not sure where to start. Am I in the right place?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      What is #CyberMentoringMonday? How can I be involved? How can I be a good mentor? How can I make sure I’m being a good mentee?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      What is WoSEC (Women of Security)? How do I join? Is it free?  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="set">
			    <a href="#">
			      Tell me about Alice and Bob Learn Application Security, Tanya’s book. What level of knowledge do I need to read it? What does it cover? Tell me everything!  
			    </a>
			    <div class="content">
			      <p> Donec tincidunt consectetur orci at dignissim. Proin auctor aliquam justo, vitae luctus odio pretium scelerisque. </p>
			    </div>
			</div>
			<div class="Reachout">
				<p>If you have any further questions, please reach out to us at <a href="mailto:hello@wehackpurple.com">hello@wehackpurple.com</a></p>
			</div>
		</div>
		
	</div>		
</section>

<?php @include('template-parts/footer.php') ?>