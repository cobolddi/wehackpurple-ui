<?php @include('template-parts/header.php') ?>

<section class="PodcastCardsBlock">
	<div class="container">
		<div class="CardsWrapper">
			<div class="TopHeading">
				<h2 class="HeadingwithYellowBorder">We Hack Purple Podcast</h2>
			</div>
			<div class="Cards">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="ImgBlock">
							<img src="assets/img/tempImg/podcast3.png" alt="">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="RightContent">
							<h3>Episode 25 with Guest Troy Hunt</h3>
							<h6>Thursday January 28th 2021, 6:00 pm PST</h6>
							<p>Learn what it's like to be an eCommerce security educator and auditor, with Talesh Seeparsan! Originally an eCommerce PHP developer, Talesh has pivoted his career into eCommerce security. Specifically building more defensible eCommerce.</p>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<!-- <a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a> -->
							</div>
							<div class="BtnWithLogo">
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
								<img src="assets/img/threadfix.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Soundcloud">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/34019569&color=0066cc"></iframe>					
			</div>
			<div class="ContentBox">
				<h4>Transcript</h4>
				<p>welcome to the we hack purple podcast where each week we interview a different guest who has a different type of job within the industry of information security i am your host tanya janka and this week we will be talking to sasha rosenbaum who is a people manager at red hat this week's episode is sponsored by ubiq security they do api encryption as a service and it's pretty sweet but i bet you're thinking we don't want to hear about all of that tanya what we want to do is meet sasha so without further ado i'm going to welcome our guest hi sasha hi tanya thank you so much for having me over i am super excited to be here today thank you i'm really happy too um definitely this has been a long time coming yeah it's definitely like we i think we've been trying to schedule this for a few months now so i'm excited it's finally happening me too and you somewhat recently changed jobs and i'm super excited to talk about your new job because how about you tell us your title and like just like a little bit about what your job is sure so yes this is interesting because when we scheduled this i think i didn't intend to even change jobs and now i did so um things happen sometimes quickly uh so um i am now a team lead on a i would define it as a technical sales team um at red hat uh so we are there to help customers basically adopt openshift and manage openshift on uh cloud providers public cloud providers and be successful with it um so yep um you could ask me more questions like oh i have so i have so many questions yeah sure for our audience super briefly could you explain what openshift is because maybe some of them don't know yeah so i so this is a great question</p> 
				<p id="text">because like um i i guess i would say the openshift is a enterprise version of kubernetes like that's my best way to sort of put it and i don't know if this is official marketing tagline up in the three weeks so you know um this is my take on it more than uh you know i i know what the official party line is but um i will say that like you know it's a redhead implementation of openshift and more right so it takes care of security it takes care of some of the cicd pipelines and stuff like that basically provides you more services on because like um i i guess i would say the openshift is a enterprise version of kubernetes like that's my best way to sort of put it and i don't know if this is official marketing tagline up in the three weeks so you know um this is my take on it more than uh you know i i know what the official party line is but um i will say that like you know it's a redhead implementation of openshift and more right so it takes care of security it takes care of some of the cicd pipelines and stuff like that basically provides you more services on because like um i i guess i would say the openshift is a enterprise version of kubernetes like that's my best way to sort of put it and i don't know if this is official marketing tagline up in the three weeks so you know um this is my take on it more than uh you know i i know what the official party line is but um i will say that like you know it's a redhead implementation of openshift and more right so it takes care of security it takes care of some of the cicd pipelines and stuff like that basically provides you more services on </p>
				<button id="toggle">Read complete transcript</button>
			</div>
		</div>
	</div>
</section>


<section class="TopContentWithThreeCards SinglePodcastCards">
	<div class="container">
		<div class="Centercontent">
			<h2 class="HeadingwithYellowBorder">We Hack Purple Podcast</h2>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempImg/podcast.png" alt="" class="Topimg">
						<div class="BottomContent">
							<h6>#1312: Melisa Benua and some title of the podcast this week.</h6>
							<div class="playTime">
								<span><img src="assets/img/clock.svg" alt="">2h 54m</span>
								<span><img src="assets/img/note.svg" alt="">Shownotes</span>
								<a href="#" class="play">Play <img src="assets/img/play.svg" alt=""></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>