<?php @include('template-parts/header.php') ?>

<section class="contactusSection">
	<div class="container">
		<div class="ContactBlock">
			<p>Question about our courses or academy? Ask us here!</p>
			<h1><a href="mailto:academy@WeHackPurple.com">Academy@WeHackPurple.com</a></h1>
			<p>Have a question about the community that you don’t want to ask IN the community? Ask us here.</p>
			<h1><a href="mailto:Community@WeHackPurple.com">Community@WeHackPurple.com</a></h1>
			<p>Technical difficulties? Let us help you.</p>
			<h1><a href="mailto:support@WeHackPurple.com">Support@WeHackPurple.com</a></h1>
			<p>Want to hire us for live training? Message us!</p>
			<h1><a href="mailto:training@WeHackPurple.com">Training@WeHackPurple.com</a></h1>
			<p>For everything else.</p>
			<h1><a href="mailto:info@WeHackPurple.com">Info@WeHackPurple.com</a></h1>
		</div>
		<div class="SocialBlock">
			<p>or follow us here</p>
			<ul class="social">
				<li class="facebook"><a href="#"><img src="assets/img/facebook-white.svg" alt=""></a></li>
				<li class="twitter"><a href="#"><img src="assets/img/twitter-white.svg" alt=""></a></li>
				<li class="youtube"><a href="#"><img src="assets/img/youtube.svg" alt=""></a></li>
				<li class="insta"><a href="#"><img src="assets/img/insta-white.svg" alt=""></a></li>
			</ul>
		</div>
		<div class="Newsletter">
			<div class="Content">
				<img src="assets/img/shadowlogo.svg" alt="">
				<h2>Join our newsletter, you won’t regret it!</h2>
				<p>Join our newsletter to receive free content, deals, invites, advance notice of new products, and so much more.</p>
				<form action="">
					<input type="email" placeholder="@ Enter your email address">
					<input type="submit" value="Join">
				</form>
			</div>
		</div>
	</div>	
</section>

<?php @include('template-parts/footer.php') ?>