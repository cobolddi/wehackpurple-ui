"use strict";

$(document).ready(function () {
  $('.HomeBanner').slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    arrows: true,
    adaptiveHeight: true,
    cssEase: 'ease',
    easing: 'swing',
    responsive: [{
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 400,
      settings: {
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $('a.dismiss').on('click', function (e) {
    $('body').addClass('close');
    e.stopPropagation();
    e.preventDefault();
  });
  $(".set > a").on("click", function (e) {
    e.preventDefault();

    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".content").slideUp(200);
      $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
    } else {
      $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
      $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this).siblings(".content").slideDown(200);
    }
  });
}); // $(window).scroll(function() {    
//     var scroll = $(window).scrollTop();
//     if (scroll >= 20) {
//         $("header").addClass("HeaderBg");
//     } else {
//         $("header").removeClass("HeaderBg");
//     }
// });

$(document).ready(function () {
  $("#toggle").click(function () {
    var elem = $("#toggle").text();

    if (elem == "Read complete transcript") {
      //Stuff to do when btn is in the read more state
      $("#toggle").text("Read Less");
      $("#text").slideDown();
    } else {
      //Stuff to do when btn is in the read less state
      $("#toggle").text("Read complete transcript");
      $("#text").slideUp();
    }
  });
});
$(document).ready(function () {
  $(".button a").click(function (e) {
    e.preventDefault();
    $(".overlay").fadeToggle(200);
    $("body").addClass("overflowhidden");
    $(this).toggleClass('btn-open').toggleClass('btn-close');
  });
});
$('.close').on('click', function (e) {
  e.preventDefault();
  $(".overlay").fadeToggle(200);
  $("body").removeClass("overflowhidden");
  $(".button a").toggleClass('btn-open').toggleClass('btn-close');
  open = false;
});
$(document).on('click', '.js-videoPoster', function (e) {
  e.preventDefault();
  var poster = $(this);
  var wrapper = poster.closest('.js-videoWrapper');
  videoPlay(wrapper);
});

function videoPlay(wrapper) {
  var iframe = wrapper.find('.js-videoIframe'); // Берем ссылку видео из data

  var src = iframe.data('src'); // скрываем постер

  wrapper.addClass('videoWrapperActive'); // подставляем в src параметр из data

  iframe.attr('src', src);
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdHMuanMiXSwibmFtZXMiOlsiJCIsImRvY3VtZW50IiwicmVhZHkiLCJzbGljayIsImRvdHMiLCJpbmZpbml0ZSIsInNwZWVkIiwic2xpZGVzVG9TaG93Iiwic2xpZGVzVG9TY3JvbGwiLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJhcnJvd3MiLCJhZGFwdGl2ZUhlaWdodCIsImNzc0Vhc2UiLCJlYXNpbmciLCJyZXNwb25zaXZlIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwib24iLCJlIiwiYWRkQ2xhc3MiLCJzdG9wUHJvcGFnYXRpb24iLCJwcmV2ZW50RGVmYXVsdCIsImhhc0NsYXNzIiwicmVtb3ZlQ2xhc3MiLCJzaWJsaW5ncyIsInNsaWRlVXAiLCJmaW5kIiwic2xpZGVEb3duIiwiY2xpY2siLCJlbGVtIiwidGV4dCIsImZhZGVUb2dnbGUiLCJ0b2dnbGVDbGFzcyIsIm9wZW4iLCJwb3N0ZXIiLCJ3cmFwcGVyIiwiY2xvc2VzdCIsInZpZGVvUGxheSIsImlmcmFtZSIsInNyYyIsImRhdGEiLCJhdHRyIl0sIm1hcHBpbmdzIjoiOztBQUFBQSxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFFNUJGLEVBQUFBLENBQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUJHLEtBQWpCLENBQXVCO0FBQ2hCQyxJQUFBQSxJQUFJLEVBQUUsSUFEVTtBQUVoQkMsSUFBQUEsUUFBUSxFQUFFLElBRk07QUFHaEJDLElBQUFBLEtBQUssRUFBRSxHQUhTO0FBSWhCQyxJQUFBQSxZQUFZLEVBQUUsQ0FKRTtBQUtoQkMsSUFBQUEsY0FBYyxFQUFFLENBTEE7QUFNaEJDLElBQUFBLFFBQVEsRUFBRSxLQU5NO0FBT2hCQyxJQUFBQSxhQUFhLEVBQUUsSUFQQztBQVFoQkMsSUFBQUEsTUFBTSxFQUFFLElBUlE7QUFTaEJDLElBQUFBLGNBQWMsRUFBRSxJQVRBO0FBVWhCQyxJQUFBQSxPQUFPLEVBQUUsTUFWTztBQVdsQkMsSUFBQUEsTUFBTSxFQUFFLE9BWFU7QUFZaEJDLElBQUFBLFVBQVUsRUFBRSxDQUFDO0FBQ1hDLE1BQUFBLFVBQVUsRUFBRSxHQUREO0FBRVhDLE1BQUFBLFFBQVEsRUFBRTtBQUNSVixRQUFBQSxZQUFZLEVBQUUsQ0FETjtBQUVSQyxRQUFBQSxjQUFjLEVBQUU7QUFGUjtBQUZDLEtBQUQsRUFPWjtBQUNHUSxNQUFBQSxVQUFVLEVBQUUsR0FEZjtBQUVHQyxNQUFBQSxRQUFRLEVBQUU7QUFDUE4sUUFBQUEsTUFBTSxFQUFFLEtBREQ7QUFFUEosUUFBQUEsWUFBWSxFQUFFLENBRlA7QUFHUEMsUUFBQUEsY0FBYyxFQUFFO0FBSFQ7QUFGYixLQVBZO0FBWkksR0FBdkI7QUE4QkdSLEVBQUFBLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWtCLEVBQWYsQ0FBa0IsT0FBbEIsRUFBMkIsVUFBU0MsQ0FBVCxFQUFZO0FBQ25DbkIsSUFBQUEsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVb0IsUUFBVixDQUFtQixPQUFuQjtBQUNBRCxJQUFBQSxDQUFDLENBQUNFLGVBQUY7QUFDQUYsSUFBQUEsQ0FBQyxDQUFDRyxjQUFGO0FBQ0gsR0FKRDtBQU9BdEIsRUFBQUEsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFja0IsRUFBZCxDQUFpQixPQUFqQixFQUEwQixVQUFTQyxDQUFULEVBQVk7QUFDbENBLElBQUFBLENBQUMsQ0FBQ0csY0FBRjs7QUFDQSxRQUFJdEIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRdUIsUUFBUixDQUFpQixRQUFqQixDQUFKLEVBQWdDO0FBQzlCdkIsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRd0IsV0FBUixDQUFvQixRQUFwQjtBQUNBeEIsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUNHeUIsUUFESCxDQUNZLFVBRFosRUFFR0MsT0FGSCxDQUVXLEdBRlg7QUFHQTFCLE1BQUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FDR3dCLFdBREgsQ0FDZSxVQURmLEVBRUdKLFFBRkgsQ0FFWSxTQUZaO0FBR0QsS0FSRCxNQVFPO0FBQ0xwQixNQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQ0d3QixXQURILENBQ2UsVUFEZixFQUVHSixRQUZILENBRVksU0FGWjtBQUdBcEIsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUNHMkIsSUFESCxDQUNRLEdBRFIsRUFFR0gsV0FGSCxDQUVlLFNBRmYsRUFHR0osUUFISCxDQUdZLFVBSFo7QUFJQXBCLE1BQUFBLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY3dCLFdBQWQsQ0FBMEIsUUFBMUI7QUFDQXhCLE1BQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUW9CLFFBQVIsQ0FBaUIsUUFBakI7QUFDQXBCLE1BQUFBLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBYzBCLE9BQWQsQ0FBc0IsR0FBdEI7QUFDQTFCLE1BQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FDR3lCLFFBREgsQ0FDWSxVQURaLEVBRUdHLFNBRkgsQ0FFYSxHQUZiO0FBR0Q7QUFDSixHQXpCRDtBQTZCSCxDQXBFRCxFLENBdUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E1QixDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFDM0JGLEVBQUFBLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYTZCLEtBQWIsQ0FBbUIsWUFBVztBQUM1QixRQUFJQyxJQUFJLEdBQUc5QixDQUFDLENBQUMsU0FBRCxDQUFELENBQWErQixJQUFiLEVBQVg7O0FBQ0EsUUFBSUQsSUFBSSxJQUFJLDBCQUFaLEVBQXdDO0FBQ3RDO0FBQ0E5QixNQUFBQSxDQUFDLENBQUMsU0FBRCxDQUFELENBQWErQixJQUFiLENBQWtCLFdBQWxCO0FBQ0EvQixNQUFBQSxDQUFDLENBQUMsT0FBRCxDQUFELENBQVc0QixTQUFYO0FBQ0QsS0FKRCxNQUlPO0FBQ0w7QUFDQTVCLE1BQUFBLENBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYStCLElBQWIsQ0FBa0IsMEJBQWxCO0FBQ0EvQixNQUFBQSxDQUFDLENBQUMsT0FBRCxDQUFELENBQVcwQixPQUFYO0FBQ0Q7QUFDRixHQVhEO0FBWUQsQ0FiRDtBQWdCQTFCLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBVTtBQUN4QkYsRUFBQUEsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlNkIsS0FBZixDQUFxQixVQUFTVixDQUFULEVBQVc7QUFDNUJBLElBQUFBLENBQUMsQ0FBQ0csY0FBRjtBQUNBdEIsSUFBQUEsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjZ0MsVUFBZCxDQUF5QixHQUF6QjtBQUNBaEMsSUFBQUEsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVb0IsUUFBVixDQUFtQixnQkFBbkI7QUFDRHBCLElBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlDLFdBQVIsQ0FBb0IsVUFBcEIsRUFBZ0NBLFdBQWhDLENBQTRDLFdBQTVDO0FBQ0YsR0FMRDtBQU1ILENBUEQ7QUFRQWpDLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWWtCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLFVBQVNDLENBQVQsRUFBVztBQUMvQkEsRUFBQUEsQ0FBQyxDQUFDRyxjQUFGO0FBQ0F0QixFQUFBQSxDQUFDLENBQUMsVUFBRCxDQUFELENBQWNnQyxVQUFkLENBQXlCLEdBQXpCO0FBQ0FoQyxFQUFBQSxDQUFDLENBQUMsTUFBRCxDQUFELENBQVV3QixXQUFWLENBQXNCLGdCQUF0QjtBQUNBeEIsRUFBQUEsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlaUMsV0FBZixDQUEyQixVQUEzQixFQUF1Q0EsV0FBdkMsQ0FBbUQsV0FBbkQ7QUFDQUMsRUFBQUEsSUFBSSxHQUFHLEtBQVA7QUFDSCxDQU5EO0FBU0FsQyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZaUIsRUFBWixDQUFlLE9BQWYsRUFBdUIsaUJBQXZCLEVBQXlDLFVBQVNDLENBQVQsRUFBWTtBQUNuREEsRUFBQUEsQ0FBQyxDQUFDRyxjQUFGO0FBQ0EsTUFBSWEsTUFBTSxHQUFHbkMsQ0FBQyxDQUFDLElBQUQsQ0FBZDtBQUNBLE1BQUlvQyxPQUFPLEdBQUdELE1BQU0sQ0FBQ0UsT0FBUCxDQUFlLGtCQUFmLENBQWQ7QUFDQUMsRUFBQUEsU0FBUyxDQUFDRixPQUFELENBQVQ7QUFDRCxDQUxEOztBQU9BLFNBQVNFLFNBQVQsQ0FBbUJGLE9BQW5CLEVBQTRCO0FBQzFCLE1BQUlHLE1BQU0sR0FBR0gsT0FBTyxDQUFDVCxJQUFSLENBQWEsaUJBQWIsQ0FBYixDQUQwQixDQUUxQjs7QUFDQSxNQUFJYSxHQUFHLEdBQUdELE1BQU0sQ0FBQ0UsSUFBUCxDQUFZLEtBQVosQ0FBVixDQUgwQixDQUkxQjs7QUFDQUwsRUFBQUEsT0FBTyxDQUFDaEIsUUFBUixDQUFpQixvQkFBakIsRUFMMEIsQ0FNMUI7O0FBQ0FtQixFQUFBQSxNQUFNLENBQUNHLElBQVAsQ0FBWSxLQUFaLEVBQWtCRixHQUFsQjtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHRcblxuXHQkKCcuSG9tZUJhbm5lcicpLnNsaWNrKHtcbiAgICAgICAgZG90czogdHJ1ZSxcbiAgICAgICAgaW5maW5pdGU6IHRydWUsXG4gICAgICAgIHNwZWVkOiA1MDAsXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcbiAgICAgICAgYXV0b3BsYXlTcGVlZDogMjAwMCxcbiAgICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgICBhZGFwdGl2ZUhlaWdodDogdHJ1ZSxcbiAgICAgICAgY3NzRWFzZTogJ2Vhc2UnLFxuICAgIFx0IGVhc2luZzogJ3N3aW5nJyxcbiAgICAgICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgICBicmVha3BvaW50OiA2MDAsXG4gICAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgIGJyZWFrcG9pbnQ6IDQwMCxcbiAgICAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMVxuICAgICAgICAgICB9XG4gICAgICAgIH1dXG4gICAgfSk7XG5cblxuICAgICQoJ2EuZGlzbWlzcycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdjbG9zZScpO1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cblxuICAgICQoXCIuc2V0ID4gYVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xuICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgICAgJCh0aGlzKVxuICAgICAgICAgICAgLnNpYmxpbmdzKFwiLmNvbnRlbnRcIilcbiAgICAgICAgICAgIC5zbGlkZVVwKDIwMCk7XG4gICAgICAgICAgJChcIi5zZXQgPiBhIGlcIilcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhcImZhLW1pbnVzXCIpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoXCJmYS1wbHVzXCIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICQoXCIuc2V0ID4gYSBpXCIpXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoXCJmYS1taW51c1wiKVxuICAgICAgICAgICAgLmFkZENsYXNzKFwiZmEtcGx1c1wiKTtcbiAgICAgICAgICAkKHRoaXMpXG4gICAgICAgICAgICAuZmluZChcImlcIilcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhcImZhLXBsdXNcIilcbiAgICAgICAgICAgIC5hZGRDbGFzcyhcImZhLW1pbnVzXCIpO1xuICAgICAgICAgICQoXCIuc2V0ID4gYVwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICQoXCIuY29udGVudFwiKS5zbGlkZVVwKDIwMCk7XG4gICAgICAgICAgJCh0aGlzKVxuICAgICAgICAgICAgLnNpYmxpbmdzKFwiLmNvbnRlbnRcIilcbiAgICAgICAgICAgIC5zbGlkZURvd24oMjAwKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG5cblxufSk7XG5cblxuLy8gJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpIHsgICAgXG4vLyAgICAgdmFyIHNjcm9sbCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcblxuLy8gICAgIGlmIChzY3JvbGwgPj0gMjApIHtcbi8vICAgICAgICAgJChcImhlYWRlclwiKS5hZGRDbGFzcyhcIkhlYWRlckJnXCIpO1xuLy8gICAgIH0gZWxzZSB7XG4vLyAgICAgICAgICQoXCJoZWFkZXJcIikucmVtb3ZlQ2xhc3MoXCJIZWFkZXJCZ1wiKTtcbi8vICAgICB9XG4vLyB9KTtcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAkKFwiI3RvZ2dsZVwiKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICB2YXIgZWxlbSA9ICQoXCIjdG9nZ2xlXCIpLnRleHQoKTtcbiAgICBpZiAoZWxlbSA9PSBcIlJlYWQgY29tcGxldGUgdHJhbnNjcmlwdFwiKSB7XG4gICAgICAvL1N0dWZmIHRvIGRvIHdoZW4gYnRuIGlzIGluIHRoZSByZWFkIG1vcmUgc3RhdGVcbiAgICAgICQoXCIjdG9nZ2xlXCIpLnRleHQoXCJSZWFkIExlc3NcIik7XG4gICAgICAkKFwiI3RleHRcIikuc2xpZGVEb3duKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vU3R1ZmYgdG8gZG8gd2hlbiBidG4gaXMgaW4gdGhlIHJlYWQgbGVzcyBzdGF0ZVxuICAgICAgJChcIiN0b2dnbGVcIikudGV4dChcIlJlYWQgY29tcGxldGUgdHJhbnNjcmlwdFwiKTtcbiAgICAgICQoXCIjdGV4dFwiKS5zbGlkZVVwKCk7XG4gICAgfVxuICB9KTtcbn0pO1xuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgJChcIi5idXR0b24gYVwiKS5jbGljayhmdW5jdGlvbihlKXtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAkKFwiLm92ZXJsYXlcIikuZmFkZVRvZ2dsZSgyMDApO1xuICAgICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhcIm92ZXJmbG93aGlkZGVuXCIpO1xuICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ2J0bi1vcGVuJykudG9nZ2xlQ2xhc3MoJ2J0bi1jbG9zZScpO1xuICAgIH0pO1xufSk7XG4kKCcuY2xvc2UnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgJChcIi5vdmVybGF5XCIpLmZhZGVUb2dnbGUoMjAwKTsgXG4gICAgJChcImJvZHlcIikucmVtb3ZlQ2xhc3MoXCJvdmVyZmxvd2hpZGRlblwiKTsgIFxuICAgICQoXCIuYnV0dG9uIGFcIikudG9nZ2xlQ2xhc3MoJ2J0bi1vcGVuJykudG9nZ2xlQ2xhc3MoJ2J0bi1jbG9zZScpO1xuICAgIG9wZW4gPSBmYWxzZTtcbn0pO1xuXG5cbiQoZG9jdW1lbnQpLm9uKCdjbGljaycsJy5qcy12aWRlb1Bvc3RlcicsZnVuY3Rpb24oZSkge1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG4gIHZhciBwb3N0ZXIgPSAkKHRoaXMpO1xuICB2YXIgd3JhcHBlciA9IHBvc3Rlci5jbG9zZXN0KCcuanMtdmlkZW9XcmFwcGVyJyk7XG4gIHZpZGVvUGxheSh3cmFwcGVyKTtcbn0pO1xuXG5mdW5jdGlvbiB2aWRlb1BsYXkod3JhcHBlcikge1xuICB2YXIgaWZyYW1lID0gd3JhcHBlci5maW5kKCcuanMtdmlkZW9JZnJhbWUnKTtcbiAgLy8g0JHQtdGA0LXQvCDRgdGB0YvQu9C60YMg0LLQuNC00LXQviDQuNC3IGRhdGFcbiAgdmFyIHNyYyA9IGlmcmFtZS5kYXRhKCdzcmMnKTtcbiAgLy8g0YHQutGA0YvQstCw0LXQvCDQv9C+0YHRgtC10YBcbiAgd3JhcHBlci5hZGRDbGFzcygndmlkZW9XcmFwcGVyQWN0aXZlJyk7XG4gIC8vINC/0L7QtNGB0YLQsNCy0LvRj9C10Lwg0LIgc3JjINC/0LDRgNCw0LzQtdGC0YAg0LjQtyBkYXRhXG4gIGlmcmFtZS5hdHRyKCdzcmMnLHNyYyk7XG59Il0sImZpbGUiOiJzY3JpcHRzLmpzIn0=
